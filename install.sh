#!/usr/bin/bash

XDG_CONFIG_HOME=$HOME/.config
PWD=$(pwd)

make_symlink() {
	SRC=$1
	DEST=$2

	if [ -e "$DEST" ]; then
		echo "Backing up: $DEST"
		mv $DEST "$DEST.old"
	fi

	ln -s $SRC $DEST
}

# Make sure xdg config directory exists
mkdir -p $XDG_CONFIG_HOME

make_symlink $PWD/xinitrc $HOME/.xinitrc
make_symlink $PWD/xprofile $HOME/.xprofile
make_symlink $PWD/Xmodmap $HOME/.Xmodmap
make_symlink $PWD/Xresources $HOME/.Xresources
make_symlink $PWD/gemrc $HOME/.gemrc

make_symlink $PWD/config/compton.conf $XDG_CONFIG_HOME/compton.conf
make_symlink $PWD/config/i3 $XDG_CONFIG_HOME/i3
make_symlink $PWD/config/polybar $XDG_CONFIG_HOME/polybar
make_symlink $PWD/config/dunst $XDG_CONFIG_HOME/dunst
make_symlink $PWD/config/mpd $XDG_CONFIG_HOME/mpd
make_symlink $PWD/config/ncmpcpp $XDG_CONFIG_HOME/ncmpcpp
make_symlink $PWD/config/rofi $XDG_CONFIG_HOME/rofi
make_symlink $PWD/config/redshift $XDG_CONFIG_HOME/redshift

mkdir -p $HOME/.mpd/playlists

# Make polybar scripts executable
chmod +x $PWD/config/polybar/launch.sh
chmod +x $PWD/config/polybar/scripts/player-ctrl.sh
chmod +x $PWD/config/polybar/scripts/player-mpris-tail.py
chmod +x $PWD/config/polybar/scripts/openweathermap-forecast.sh

echo "Done."
